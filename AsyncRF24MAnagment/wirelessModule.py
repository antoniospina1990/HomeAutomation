#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Example program to send packets to the radio link
#


import RPi.GPIO as GPIO
#GPIO.cleanup()
GPIO.setmode(GPIO.BOARD)
#from RF24 import *
import time as time
#import spidev
from RF24 import *
import sys
from array import array
import ctypes
#radio =RF24()

class RF24MANAGMENT():

        @staticmethod
	def __init__():
                print("--------INIT RF24 DEVICES -----------")
                #pipes = [[0xf0,0xf0,0xf0,0xf0,0xd2],[0xf0,0xf0,0xf0,0xf0,0xe1]]
		pipes = [ 0xF0F0F0F0E1, 0xF0F0F0F0D2]
		#spi = spidev.SpiDev()
		#spi.open(0, 1)
		#spi.max_speed_hz = 7629
		#spi.max_speed_hz = 8000
		
		#radio = RF24(RPI_V2_GPIO_P1_22, RPI_V2_GPIO_P1_26, BCM2835_SPI_SPEED_8MHZ)
		global radio 
		radio = RF24(RPI_BPLUS_GPIO_J8_22,RPI_BPLUS_GPIO_J8_26, BCM2835_SPI_SPEED_8MHZ)
			#radio = NRF24(GPIO, spidev.SpiDev())
		#radio = NRF24(GPIO, spi)
		#radio = NRF24()
		radio.begin()
		time.sleep(0.1)
		radio.setRetries(15,15)
		#radio.setPayloadSize(32)
		#radio.setChannel(0x60)
		#radio.setDataRate(NRF24.BR_2MBPS)
		#radio.setPALevel(NRF24.PA_MAX)
		#radio.setAutoAck(True)
		#radio.enableDynamicPayloads()
		#radio.enableAckPayload()
		radio.openWritingPipe(pipes[0])
		radio.openReadingPipe(1, pipes[1])
		radio.printDetails()
	
	@staticmethod
	def sendMessage(action):
                global radio
	#        global ftp
	        #print(action)
	        #global radio
	        radio.stopListening()
	#        string_buffers = [ctypes.create_string_buffer(8) for i in range(4)]
	#        pointers = (ctypes.c_ulong*4)(*map(ctypes.addressof, string_buffers))
	#        message=ctypes.c_ulong(int(action))
	#        print(ctypes.c_ulong(int(message)))
	       # ok = radio.write(message)
	        message = int(action)
	        ok = radio.write(message)
	        #print("Now sending :" + str(message))
	        #print(ok)
	        #if not ok:
	        #   print("failed...")
	        #else:
	        #   print("ok!")
	        #print("arrivo qua")
	        radio.startListening()
		#Let's take the time while we listen
                time.sleep(0.3)
	        started_waiting_at = int(round(time.time() * 1000))
	        timeout = False
	        #print(radio.available())
	        #radio.read( got_message, radio2.getDynamicPayloadSize())
	        #print("Yay! Got this response ",got_message)
	        while ( not radio.available() and  not timeout ):
	                #print("%d", !radio.available())
	                if ((int(round(time.time() * 1000)) - started_waiting_at) > 500 ):
	                        timeout = True
	                if ( timeout ):
	                        #If we waited too long the transmission failed
	                        #print("Oh gosh, it's not giving me any response...")
	                        return -1
	        else:
	        #If we received the message in time, let's read it and print it
	                got_message=bytearray(radio.read())
	                print("Got this response " +str(got_message[0]))
	#                arr = bytearray(got_message)
	#                for value in arr:
	#                         print(value)
	#                print(str(bytearray(got_message)))
	                return got_message[0]
		
	
	
	
	
	
	def main():
	        Init()
	        counter = 0
	        switched = False
		#Define the options
	        choice = sys.argv[1]
	        print(choice)
	        #while(( choice == getopt( argc, argv, "m:")) != -1)
	        while choice == 'm':
	                if (choice == 'm'):
	                 print(' Talking with my NRF24l01+ friends out there....')
        	         while not switched  and counter < 5:
	                                #print(sys.argv[1])
	                                switched = sendMessage(sys.argv[2])
	                                counter = counter + 1
	                                time.sleep(1)
	                else:
	                        print("\n\rIt's time to make some choices...\n")
	                        print("\n\rTIP: Use -m idAction for the message to send. ")
	                        print("\n\rExample (id number 12, action number 1): ")
	                        print("\nsudo ./remote -m 121\n")
	                        #return 0 if everything went good, 2 otherwise
	                if (counter < 5):
	                        return 0
	                else:
	                        return 2
	

#try:
#    main()
#    GPIO.cleanup()

#except KeyboardInterrupt:
#    GPIO.cleanup()

