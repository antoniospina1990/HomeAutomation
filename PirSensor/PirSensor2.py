#  libreria python per controllo allarme
#  03092016 - aspina - aggiunto controllo device in wifi per accensione luce o abilitazione allarme
#
from time import sleep
from threading import Thread 
import pygame
import sqlite3
import time, datetime
import RPi.GPIO as GPIO
import os
import subprocess
# metodo utitlizzato per scattare foto
def Pic():
    global timestamp
    os.system("fswebcam -d /dev/video0 -r 640x480 /home/pi/Documents/PirSensor/Snaps/Pic_1_" + timestamp.replace(' ','__').replace(':','_') + ".jpeg")
    #time.sleep(700)
    #os.system("fswebcam -d /dev/video0 ~/Documents/PirSensor/Snaps/Pic_2_" +  timestamp.replace(' ','__').replace(':','_') + ".jpeg")
    #time.sleep(700)
    #os.system("fswebcam -d /dev/video0 ~/Documents/PirSensor/Snaps/Pic_3_" +  timestamp.replace(' ','__').replace(':','_') + ".jpeg")
    return
# metodo utitlizzato per far suonare l'allarme 
def Play():
   #print("Alarm Playing..." +  str(GPIO.input(4)))
#   while str(GPIO.input(4)) == '1':
   #os.system("fswebcam -d /dev/video0 ~/Documents/PirSensor/Snaps/Pic_" + time.strftime('%Y-%m-%d__%H-%M-%S') + ".jpeg")
   pygame.mixer.init()
   pygame.mixer.music.load("/home/pi/Documents/PirSensor/ALARM.wav")
   pygame.mixer.music.play()
#       while pygame.mixer.music.get_busy() == True:
#           continue
   return


GPIO.setmode(GPIO.BCM)     # set up BCM GPIO numbering
print(':: Welcome to PirSensor 1.2 ::' + '\n' + ':: author : Antonio Spina' )

#Create Db if it doesn't exist
dbName = 'PirSensor.db'
conn = sqlite3.connect(r"/home/pi/Documents/PirSensor/"+ dbName)
try:
      #conn = sqlite3.connect(r"/home/pi/Documents/PirSensor/"+ dbName)
      #print("DB CREATED - CONNECTION HAS BEEN OPENED")
      conn.execute('''CREATE TABLE PEOPLE_DETECTIONS
     (ID             INTEGER PRIMARY KEY AUTOINCREMENT,
      DATE_TIME      DATETIME    NOT NULL,
      NOTE           CHAR(500),
      IMAGE          CHAR(500),
      LEVEL          CHAR(10));''');
      print("TABLE HAS BEEN CREATED")
      conn.commit()
      print("INFORMATIONS HAVE BEEN COMMITTED")
     # conn.close()
     # print("CONNECTION HAS BEEN CLOSED")
except Exception as e: 
#      print(str(e))
#      print(str(datetime.datetime.now()-datetime.timedelta(days=180)))
      if ('already exists' in str(e) and 'table' in str(e)):
           conn.execute("""DELETE FROM PEOPLE_DETECTIONS WHERE DATE_TIME < ? """,(str(datetime.datetime.now()-datetime.timedelta(days=90)),))
           conn.commit()
           print("OLD INFORMATIONS HAVE BEEN DELETED")
conn.close()
#print("CONNECTION HAS BEEN CLOSED")
# Define GPIO to use on Pi
hostname = "192.168.1.240"
pingArgs = ["ping", "-c", "1", hostname]
TimeDelay = 20
TIME = time.time() - TimeDelay
#TIME = datetime.now() - datetime.timedelta(seconds=3) 
GPIO_PIR = 20
GPIO_LED = 17
GPIO_LIGHT = 26
GPIO.setup(GPIO_PIR,GPIO.IN)      # Echo
#set up LED output
GPIO.setup(GPIO_LED, GPIO.OUT)
GPIO.setup(GPIO_LIGHT, GPIO.OUT)
#global AlarmArmed
Current_State  = 0
Previous_State = 0

GPIO.setup(4, GPIO.OUT)
GPIO.setup(21, GPIO.IN, pull_up_down=GPIO.PUD_UP)
AlarmArmed = 1
GPIO.output(4, GPIO.HIGH)
timestamp = time.strftime('%Y-%m-%d %H:%M:%S')

# Callback function to run when motion detected
def AlarmStatus(channel):
    global AlarmArmed
    #GPIO.output(4, GPIO.HIGH)
    #print(AlarmArmed)
    if AlarmArmed == 1:     # True = Rising
        AlarmArmed = 0
        #DateTime = time.strftime('%Y-%m-%d %H:%M:%S')
        print("Alarm disarmed - Date: " + time.strftime('%Y-%m-%d %H:%M:%S'))
        GPIO.output(4, GPIO.LOW)
        GPIO.output(GPIO_LED, GPIO.LOW)
        GPIO.output(GPIO_LIGHT, GPIO.LOW)
        TIME = time.time() - TimeDelay
        conn = sqlite3.connect(r"/home/pi/Documents/PirSensor/"+ dbName)
        #print("INSERT INTO PEOPLE_DETECTION (DATE_TIME,INFORMATION,LEVEL) VALUES ('" + DateTime + "', '', 'I')");
        parameters=(time.strftime('%Y-%m-%d %H:%M:%S'),'Alarm disarmed','W')
        conn.execute("INSERT INTO PEOPLE_DETECTIONS (DATE_TIME, NOTE, LEVEL) VALUES (?, ?, ?) ", parameters)
        conn.commit()
        conn.close()
    else:
        AlarmArmed = 1
        #DateTime = time.strftime('%Y-%m-%d %H:%M:%S')
        print("Alarm armed - Date: " + time.strftime('%Y-%m-%d %H:%M:%S'))
        GPIO.output(4, GPIO.HIGH)
        conn = sqlite3.connect(r"/home/pi/Documents/PirSensor/"+ dbName)
        #print("INSERT INTO PEOPLE_DETECTION (DATE_TIME,INFORMATION,LEVEL) VALUES ('" + DateTime + "', '', 'I')");
        parameters=(time.strftime('%Y-%m-%d %H:%M:%S'),'Alarm armed','W')
        conn.execute("INSERT INTO PEOPLE_DETECTIONS (DATE_TIME, NOTE, LEVEL) VALUES (?, ?, ?) ", parameters)
        conn.commit()
        conn.close()

#add event listener on pin 21
GPIO.add_event_detect(21, GPIO.BOTH, callback=AlarmStatus, bouncetime=3000)


 
try:
# if AlarmArmed == 1:

#  error = ping.communicate()
#  print(TIME)
  print ("Waiting for PIR to settle ...")
 
  # Loop until PIR output is 0
  while GPIO.input(GPIO_PIR)==1:
    Current_State  = 0
 
  print ("  Ready")
 
  # Loop until users quits with CTRL-C
  while True :
   if time.time() > (TIME + TimeDelay):
        GPIO.output(GPIO_LIGHT, GPIO.LOW) 
#   print(TIME)
   if AlarmArmed == 1:
    # Read PIR state
    Current_State = GPIO.input(GPIO_PIR) 
    if Current_State==1 and Previous_State==0:
        try:
            ping = subprocess.check_output(pingArgs,timeout=0.5)
        except:
            ping = False
        else:
            ping = True
#        print(ping)	
    if Current_State==1 and Previous_State==0 and not ping and time.time() > (TIME + TimeDelay):
      # PIR is triggered
        timestamp =  time.strftime('%Y-%m-%d %H:%M:%S')
        print("Detection acquired - Date: " + timestamp)
        GPIO.output(GPIO_LED, GPIO.HIGH)
        Thread(target=Pic).start() 
        Play()       
#        Thread(target=Play).start()
        conn = sqlite3.connect(r"/home/pi/Documents/PirSensor/"+ dbName)
        #print("INSERT INTO PEOPLE_DETECTION (DATE_TIME,INFORMATION,LEVEL) VALUES$
#        parameters=(time.strftime('%Y-%m-%d %H:%M:%S'),'DETECTION','I')
        parameters=(timestamp,'DETECTION','I','Pic_1_' + timestamp.replace(' ','__').replace(':','_') + ".jpeg")
        conn.execute("INSERT INTO PEOPLE_DETECTIONS (DATE_TIME, NOTE, LEVEL, IMAGE) VALUES (?, ?, ?, ?) ", parameters)
        conn.commit()
        conn.close()
#        print('end commit')
      # Record previous state
        Previous_State=1
    elif Current_State==1 and Previous_State==0 and ping and time.time() > (TIME + TimeDelay):
#        print("sono qui!")
        TIME = time.time()
#        print(str(TIME))
        if not GPIO.input(GPIO_LIGHT):
#            print("accendo il led")		
            GPIO.output(GPIO_LIGHT, GPIO.HIGH)
        conn = sqlite3.connect(r"/home/pi/Documents/PirSensor/"+ dbName)
           #print("INSERT INTO PEOPLE_DETECTION (DATE_TIME,INFORMATION,LEVEL) VALUES ('"$
        parameters=(time.strftime('%Y-%m-%d %H:%M:%S'),'LIGHT_SWITCHED_ON','I')
        conn.execute("INSERT INTO PEOPLE_DETECTIONS (DATE_TIME, NOTE, LEVEL) VALUES (?, ?, ?) ", parameters)
        conn.commit()
        conn.close()
        print("Light On!")
    elif Current_State==0 and Previous_State==1:
      print('Ready')
      # PIR has returned to ready state
      GPIO.output(GPIO_LED, GPIO.LOW)
      Previous_State=0
 
    # Wait for 10 milliseconds
  time.sleep(0.01)
 
except KeyboardInterrupt:
    conn = sqlite3.connect(r"/home/pi/Documents/PirSensor/"+ dbName)
    #print("INSERT INTO PEOPLE_DETECTION (DATE_TIME,INFORMATION,LEVEL) VALUES ('"$
    parameters=(time.strftime('%Y-%m-%d %H:%M:%S'),'APPLICATION QUITTING...','W')
    conn.execute("INSERT INTO PEOPLE_DETECTIONS (DATE_TIME, NOTE, LEVEL) VALUES (?, ?, ?) ", parameters)
    conn.commit()
    conn.close()
    print ("All cleaned up.")
    # Reset GPIO settings
    GPIO.cleanup()



		
# Set up input pin
# GPIO.setup(7, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
# Set up LED output
# GPIO.setup(4, GPIO.OUT)

# Callback function to run when motion detected
# def motionSensor(channel):
#    GPIO.output(4, GPIO.LOW)
#    if GPIO.input(7):     # True = Rising
#        #DateTime = time.strftime('%Y-%m-%d %H:%M:%S')
#        print("Detection acquired - Date: " + time.strftime('%Y-%m-%d %H:%M:%S'))
#        GPIO.output(4, GPIO.HIGH)
#        Play()
#        conn = sqlite3.connect(r"/home/pi/Documents/PirSensor/"+ dbName)
#        #print("INSERT INTO PEOPLE_DETECTION (DATE_TIME,INFORMATION,LEVEL) VALUES ('" + DateTime + "', '', 'I')");
#        parameters=(time.strftime('%Y-%m-%d %H:%M:%S'),'DETECTION','I')
#	conn.execute("INSERT INTO PEOPLE_DETECTIONS (DATE_TIME, NOTE, LEVEL) VALUES (?, ?, ?) ", parameters)
#        conn.commit()
#        conn.close()
# add event listener on pin 21
#GPIO.add_event_detect(7, GPIO.BOTH, callback=motionSensor, bouncetime=300) 
#
#try:
#    while True:
#        sleep(0.1)         # wait 1 second
#finally:                   # run on exit
#    GPIO.cleanup()         # clean up
#    conn = sqlite3.connect(r"/home/pi/Documents/PirSensor/"+ dbName)
#    #print("INSERT INTO PEOPLE_DETECTION (DATE_TIME,INFORMATION,LEVEL) VALUES ('" + DateTime + "', '', 'I')");
#    parameters=(time.strftime('%Y-%m-%d %H:%M:%S'),'APPLICATION QUITTING...','W')
#    conn.execute("INSERT INTO PEOPLE_DETECTIONS (DATE_TIME, NOTE, LEVEL) VALUES (?, ?, ?) ", parameters)
#    conn.commit()
#    conn.close()
#    print "All cleaned up."
