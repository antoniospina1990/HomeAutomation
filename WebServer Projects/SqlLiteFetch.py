#!flask/bin/python
#
# Web server utilizzato per la gestione dei pin GPIO da remoto
# Author : Antonio Spina
#
from flask import Flask, jsonify, redirect, url_for, render_template, request, session, Response
#import RPi.GPIO as GPIO
#import hashlib
import time, json
from collections import namedtuple
#import asyncio
import os
import sqlite3
#from  wirelessModule import *
dbName = 'PirSensor.db'
conn = sqlite3.connect(r"/home/pi/Documents/PirSensor/"+ dbName, check_same_thread=False)
conn.text_factory = str
app = Flask(__name__)



@app.route('/')
def index():
    return redirect(url_for('Info'))


@app.route('/Info')
def Info():
    return "Web Service used to get information from sqlite db.<br>Author Antonio Spina"
    #return render_template('AboutMe.html')

@app.route('/GetAvailableDates', methods=['GET'])
def GetAvailableDates():
        c = conn.cursor()
        #print("INSERT INTO PEOPLE_DETECTION (DATE_TIME,INFORMATION,LEVEL) VALUES ('" + DateTime + "', '', 'I')");
        Dates = """SELECT  distinct STRFTIME('%Y-%m-%d',DATE_TIME) as DATE FROM PEOPLE_DETECTIONS ORDER BY DATE"""
#        MaxDateQuery = """SELECT STRFTIME('%d-%m-%Y', MAX(DATE_TIME)) FROM PEOPLE_DETECTIONS"""    
#        Dati = c.execute(MinDateQuery).fetchone(),c.execute(MaxDateQuery).fetchone()
        c.execute(Dates)
#        for row in c.fetchall():
#		     print row
#        Dates = c.fetchall()
#        print(Dates)
        Dates=[row[0] for row in c.fetchall()]
#        for row in Dates:
#		     print str(row)
        resp = Response(json.dumps({"Dates": Dates}, sort_keys=True,indent=4, separators=(',', ': ')))
        resp.headers['Access-Control-Allow-Origin'] = '*'
        return resp
#        return MinDateQuery,MaxDateQuery

@app.route('/GetStatistics', methods=['GET'])
def GetStatistics():
    DateFrom = request.args.get('DateFrom','0')
    DateTo = request.args.get('DateTo','0') 
    HourFrom = request.args.get('HourFrom','00')
    HourTo = request.args.get('HourTo','23')
    if 	DateFrom == DateTo and HourFrom == HourTo =='00':
	         HourTo = '23'             
#    print DateFrom, DateTo, HourFrom, HourTo
    c = conn.cursor()
#SELECT * FROM PEOPLE_DETECTIONS WHERE DATE_TIME >=DATETIME('2016-09-04 00:00:00') AND DATE_TIME < DATETIME('2016-09-04 23:59:59')
    Record = """SELECT * FROM PEOPLE_DETECTIONS WHERE DATE_TIME >=DATETIME('"""  + DateFrom + """ """ +HourFrom + """:00:00') AND DATE_TIME < DATETIME('""" +DateTo+ """ """+HourTo+""":59:59')"""
#    print Record
    c.execute(Record)
#    Record = []
#    appoo = []
#    row = c.fetchone()
#    MyStruct = namedtuple("MyStruct", "ID DATE_TIME NOTE IMAGE LEVEL")
#    while row is not None:
#        appoo.append(row[0])
#        appoo.append(row[1])
#        appoo.append(row[2])
#        appoo.append(row[3])		
#        Record.append("("+str(row[0])+","+str(row[1])+","+str(row[2])+","+str(row[3])+","+str(row[4])+")")
#        Record = json.dumps(appoo)
#        print Record
#        row = c.fetchone()
    Record = c.fetchall()
#    print Record
#    MyStruct = namedtuple("MyStruct", "ID DATE_TIME NOTE IMAGE LEVEL")
#    for row in c:
#	      Record = MyStruct(ID = row[0], DATE_TIME =row[1], NOTE = row[2], IMAGE = row[3], LEVEL = row[4])
#          print m
#    print c.fetchall()
#    Record=[row[0] for row in c.fetchall()]
#    print Record
#    resp = Response(json.dumps({"""["ID","DATE_TIME","NOTE","IMAGE","LEVEL"]""": Record}, sort_keys=True,indent=4, separators=(',', ':')))
    resp = Response(json.dumps({"Record": Record}, sort_keys=True,indent=4, separators=(',', ':')))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp



if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5010,threaded=True)
