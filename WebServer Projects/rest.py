#!flask/bin/python
#
# Web server utilizzato per la gestione dei pin GPIO da remoto
# Author : Antonio Spina
#
from flask import Flask, jsonify, redirect, url_for, render_template, request, session
import RPi.GPIO as GPIO
import hashlib
import time
#import asyncio
import os
from  wirelessModule import *


app = Flask(__name__)
RF24 = RF24MANAGMENT()
#print("init")

GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)
LocalTokenKey = "antoniospina1990"
PinList = [3,5,7,11,13,19,21,23,8,10,12,18,22,24,26]
WirelessPin = [1,2,3,4,5,6,7,8,9]
WirelessBoard =  [1,2,3,4,5,6,7,8,9]


@app.route('/')
def index():
    return redirect(url_for('AboutMe'))


@app.route('/AboutMe')
def AboutMe():
    #return "Multifunctional Web Service by Python.<br>Author Antonio Spina"
    return render_template('AboutMe.html')


@app.route('/All', methods=['POST'])
def All():
    #15 e 16 riservati per IR gestione Condizionatore
    global PinList# = [3,5,7,11,13,19,21,23,8,10,12,18,22,24,26]
    global WirelessPin# = [1,2,3,4,5,6,7,8,9]
    global WirelessBoard# =  [1,2,3,4,5,6,7,8,9]
    Set = request.args.get('Set')
    Pin = request.args.get('Pin','0')
    Token = request.args.get('Token')
    Delay = request.args.get('Delay','100')
    Wpin = request.args.get('Wpin','0')
    Wboard = request.args.get('Wboard','0')
#    print(Wpin +"   |   " + Wboard)
    #print(Delay)
    #print(Set + Pin + Token)
    #if int(Pin) in Pins:
    #    print("ci sono")
 #   print(Verify(Token))
    if (Verify(Token)):# and (int(Pin) in Pins): #or ( int(wpin) in WirelessPin and int(wboard) in WirelessBoard) ):
       if (int(Pin) in PinList):
           PinNumber = int(Pin)
           try:
            GPIO.setup(PinNumber, GPIO.OUT)
            if Set == 'On':
                GPIO.output(PinNumber,GPIO.HIGH)
            if Set == 'Off':
                GPIO.output(PinNumber,GPIO.LOW)
            if Set == 'Pulse':
                GPIO.output(PinNumber,GPIO.HIGH)
                time.sleep(float(Delay)/1000)
                GPIO.output(PinNumber,GPIO.LOW)
            if Set == 'PulseLow':
                GPIO.output(PinNumber,GPIO.LOW)
                time.sleep(float(Delay)/1000)
                GPIO.output(PinNumber,GPIO.HIGH)
            if Set == 'Clean':
                GPIO.cleanup()
           except Exception as e:
             return jsonify({'OperationStatus': {"IdPin": str(PinNumber),"Status": "Fail","Exception":str(e),"OperationType":Set}})
           return jsonify({'OperationStatus': {"IdPin": str(PinNumber),"Status": "Success","Exception":"NULL","OperationType":Set}})
       else:
           try:
               op = (int(Wpin) in WirelessPin and int(Wboard) in WirelessBoard)
               #print("try vonversion..."+ str(op))
           except Exception as e:
               return jsonify({'OperationStatus': {"IdBoard":str(Wboard),"IdPin": str(Wpin),"Status": "Fail","Exception":"Wrong pin\board selected","OperationType":Set}})
           if (op):	
               counter = 0
               res = -1
               try:
                  while( res == -1 and  int(counter) < 5):
                    # print("dentro il while " +str(res == -1 and  int(counter) < 5) +"   QQQQ   " + str( int(counter) < 5))
                     #print(res)
     	            #GPIO.setup(PinNumber, GPIO.OUT)
                     if Set == 'On':
                        res =  RF24.sendMessage(int(str(Wboard)+str(Wpin)+str(1)))
                     if Set == 'Off':
                        res =  RF24.sendMessage(int(str(Wboard)+str(Wpin)+str(0)))
                     if Set == 'Status':
                        res =  RF24.sendMessage(int(str(Wboard)+str(Wpin)+str(2)))
                     time.sleep(0.3)
                     counter=counter + 1
                     #print("dopo rf24" + str(res))
#                     print(counter+"     |    " + res)
#	            if Set == 'Pulse':
#	                GPIO.output(PinNumber,GPIO.HIGH)
#	                time.sleep(float(Delay)/1000)
#	                GPIO.output(PinNumber,GPIO.LOW)
#	            if Set == 'PulseLow':
#	                GPIO.output(PinNumber,GPIO.LOW)
#	                time.sleep(float(Delay)/1000)
#	                GPIO.output(PinNumber,GPIO.HIGH)
#	            if Set == 'Clean':
#	                GPIO.cleanup()
               except Exception as e:
                    return jsonify({'OperationStatus': {"IdBoard":str(Wboard),"IdPin": str(Wpin),"Status": "Fail","Exception":str(e),"OperationType":Set}})
               #print(res)
               if (int(res) > -1):
                    return jsonify({'OperationStatus': {"IdBoard":str(Wboard),"IdPin": str(Wpin),"Status": res,"Exception":"NULL","OperationType":Set}})
               else :
                    return jsonify({'OperationStatus': {"IdBoard":str(Wboard),"IdPin": str(Wpin),"Status": "Fail","Exception":"Something went wrong! I was unable to do the operation","OperationType":Set}})
           else:
               return jsonify({'OperationStatus': {"IdBoard":str(Wboard),"IdPin": str(Wpin),"Status":"Fail","Exception":"Wrong pin\board selected","OperationType":Set}})
    else:
          return jsonify({'OperationStatus': {"IdPin": str(Pin),"Status": "Fail","Exception":"Wrong Token! please check with the administrator","OperationType":Set}})




@app.route('/GetPinListStatus', methods=['GET'])
def GetPinListStatus():
    PinStatus = []
    #15 e 16 riservati per IR gestione Condizionatore
    global  PinList# = [3,5,7,11,13,19,21,23,8,10,12,18,22,24,26] 
    for p in PinList:
        GPIO.setup(p, GPIO.OUT)
        #print(str(p) + " Value = " + str(GPIO.input(p)))
        PinStatus.append({"IdPin": str(p),"PinValue": str(GPIO.input(p))})
    return jsonify({'PinListStatus': PinStatus})

@app.route('/GetPinList', methods=['GET'])
def GetPinList():
    PinListToSend = []
    #15 e 16 riservati per IR gestione Condizionatore
    global Pinlist# = [3,5,7,11,13,19,21,23,8,10,12,18,22,24,26] 
    for p in Pin:
        #GPIO.setup(p, GPIO.IN)
        #print(str(p) + " Value = " + str(GPIO.input(p)))
        PinListToSend.append({"IdPin": str(p)})
    return jsonify({'PinList': PinListToSend})

@app.route('/GetPinStatus', methods=['GET'])
def GetPinStatus():
    Pin = request.args.get('Pin','0')
    GPIO.setup(int(Pin), GPIO.OUT)
    #print(PinStatus)
    #PinList = []
    #Pin = [3,5,7,11,13,15,19,21,23,8,10,12,16,18,22,24,26] 
    #print(str(p) + " Value = " + str(GPIO.input(p)))
    Pin=({"IdPin": str(Pin),"PinValue": str(GPIO.input(int(Pin)))})
    return jsonify({'PinStatus': Pin})

def Verify(Token):
    global LocalTockenKey
    md5String = hashlib.md5()
    millis = int(round(time.time() * 1000))
    #print(str(millis))
    #print(str(millis)[-6:-1])
    md5String.update(LocalTokenKey.encode('utf-8'))  
    #print(md5String.hexdigest())
    Position = Token.find(md5String.hexdigest()) 
    if Position >= 0:
        #Milliseconds = Token[32:]
        check = True
    else:
        check = False
    #current_milli_time = lambda: int(round(time.time() * 1000))
    #current_milli_time = int(round(time.time() * 1000))
    #print(current_milli_time)
    #print(str(check) + "   Posi= "+ str(Position))
    return check
@app.route('/Air', methods=['GET'])
def Air():
    #Command = request.args.get('Command',' ')
    #print(Command)
    return redirect(url_for('AirLogin'))

@app.route('/AirLogin', methods=['POST','GET'])
def AirLogin():
    User = 'Antonio'
    Pass = '170490'
    Username = request.form.get('Username',type=str)
    Password = request.form.get('Password',type=str)    
    #print(User +'=='+ Username +' and'+ Pass+' =='+ Password)
    if (User == Username and Pass == Password) :
        return render_template('AirConditioningManagment.html')
    else:
        if (Username is None and Password is None):
              return render_template('Login.html')
        else:
              return render_template('Login.html',Error='Wrong username or password')

@app.route('/AirCommands', methods=['POST'])
def AirCommands():
    Command = request.form.get('submit',type=str)
    #print(Command)
    Degrees = request.form.get('Degrees',type=str)
    if Command == 'Off':
        os.system("irsend SEND_ONCE SEKOM " + Command)
    else:
        #print("irsend SEND_ONCE SEKOM " + Command+Degrees)
        os.system("irsend SEND_ONCE SEKOM " + Command+Degrees)
    #session['Command'] = "STATUS-->" + Command
    return render_template('AirConditioningManagment.html')

@app.route('/LogAlarm', methods=['GET'])
def LogAlarm():
    return render_template('LogAlarm.html')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000,threaded=True)

